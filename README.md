# Overview

A demo application that lets you manage patents in a decentralized way
This project uses several concepts (Event Driven Architecture, Domain Driven Design, Microservices and Blockchain)

* Microservices submodule

# Continuous integration

The gitlab allows to launch the tests automatically on the project
[pipeline](https://gitlab.com/matkt/patent-filing-microservices/pipelines)