/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patent.manager.rest.service.filing;

import com.patent.manager.rest.service.filing.domain.Patent;
import com.patent.manager.rest.service.filing.infrastructure.repository.PatentMongoDBRepository;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.web3j.crypto.Hash;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.web3j.crypto.Hash.sha3String;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@EmbeddedKafka
public class PatentControllerTests {

    private static final String SAMPLE_IPFS = "AD56B6D71BD1C273F91097297D1A5812B93F260BF3C78D0F84B80A7255720078";
    private static final String SAMPLE_DESCRIPTION = "description";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatentMongoDBRepository mongoDBRepository;

    @Test
    public void callToGetPatentShouldReturnAvailablePatent() throws Exception {

        Mockito.when(mongoDBRepository.findByIpfs(ArgumentMatchers.eq(SAMPLE_IPFS)))
                .then(ignoredInvocation -> new Patent(
                        sha3String(SAMPLE_IPFS),SAMPLE_IPFS, SAMPLE_DESCRIPTION)
                );

        this.mockMvc.perform(get("/getPatent").param("ipfs", SAMPLE_IPFS))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.ipfs").value(SAMPLE_IPFS));
    }

    @Test
    public void callToGetPatentWithoutIpfsShouldReturn400Error() throws Exception {

        Mockito.when(mongoDBRepository.findByIpfs(ArgumentMatchers.eq(SAMPLE_IPFS)))
                .then(ignoredInvocation -> new Patent(
                        sha3String(SAMPLE_IPFS),SAMPLE_IPFS, SAMPLE_DESCRIPTION)
                );

        this.mockMvc.perform(get("/getPatent"))
                .andDo(print()).andExpect(status().isBadRequest());
    }

}
