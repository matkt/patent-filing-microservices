package com.patent.manager.rest.service.filing.infrastructure.broker;

import com.patent.manager.rest.service.filing.usecase.PatentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
/**
 * Consumer of the KAFKA broker
 */
public class MessageConsumer {

    @Autowired
    PatentService patentService;

    @KafkaListener(topics = "patentTopic")
    public void processMessage(Message message) {
        System.out.println("Received broker [" + message + "]");
        switch (MessageType.valueOf(message.getMessageType())) {
            case PATENT_APPROVED:
                break;
            case PATENT_RECORDED:
                patentService.linkPatentToBlockchain(
                        format("0x%s", message.getPayload()),
                        message.getTransactionHash()
                );
                break;
        }
    }

}
