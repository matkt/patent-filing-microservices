package com.patent.manager.rest.service.filing.infrastructure.tracing;

import brave.Tracing;

import brave.opentracing.BraveTracer;

import io.opentracing.Tracer;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;

import zipkin2.Span;
import zipkin2.codec.Encoding;
import zipkin2.reporter.AsyncReporter;
import zipkin2.reporter.Reporter;
import zipkin2.reporter.Sender;
import zipkin2.reporter.kafka11.KafkaSender;


@Configuration
public class TracingConfiguration {


    @Value("${spring.application.name}")
    private String springApplicationName;

    @Value("${spring.kafka.bootstrap-servers}")
    private String brokerUrl;

    @Value("${spring.kafka.topic.opentracing}")
    private String opentracingTopic;


    @Bean
    public Sender sender() {
        return KafkaSender
                .newBuilder()
                .encoding(Encoding.JSON)
                .bootstrapServers(brokerUrl)
                .topic(opentracingTopic)
                .build();
    }


    @Bean
    @Autowired
    public Reporter<Span> spanReporter(Sender sender) {
        return AsyncReporter.create(sender);
    }


    @Bean
    @Autowired
    public Tracing tracing(Reporter<Span> spanReporter) {
        return Tracing
                .newBuilder()
                .localServiceName(springApplicationName)
                .spanReporter(spanReporter)
                .build();
    }


    @Bean
    @Autowired
    public Tracer tracer(Tracing tracing) {
        return BraveTracer.create(tracing);
    }

}