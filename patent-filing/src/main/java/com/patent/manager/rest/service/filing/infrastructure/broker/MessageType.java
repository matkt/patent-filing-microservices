package com.patent.manager.rest.service.filing.infrastructure.broker;


/**
 * Define the type of a KAFKA broker
 */
public enum MessageType {

    PATENT_APPROVED,
    PATENT_RECORDED;

}
