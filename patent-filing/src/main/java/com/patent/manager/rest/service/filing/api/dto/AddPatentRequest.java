package com.patent.manager.rest.service.filing.api.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(description = "Request DTO which contains all the information needed to create a new patent ")
public class AddPatentRequest {
    @NotNull
    @NotEmpty
    @Size(max = 50)
    @ApiModelProperty(notes = "Name of the Patent")
    String name;
    @NotNull
    @NotEmpty
    @ApiModelProperty(notes = "Binary of the patent file")
    String content;
    @NotNull
    @NotEmpty
    @Size(max = 200)
    @ApiModelProperty(notes = "Description of the patent")
    String description;
}
