package com.patent.manager.rest.service.filing.domain.exception;

import com.patent.manager.rest.service.filing.error.PatentException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

@ResponseStatus(HttpStatus.CONFLICT)
public class PatentCreationException extends PatentException {

    private static final String MESSAGE_TEMPLATE = "patent with name = %s cannot be added to the database";

    public PatentCreationException(String name) {
        super(format(MESSAGE_TEMPLATE, name));
    }
}
