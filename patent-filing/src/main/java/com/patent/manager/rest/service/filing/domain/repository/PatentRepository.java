package com.patent.manager.rest.service.filing.domain.repository;

import com.patent.manager.rest.service.filing.domain.Patent;
import com.patent.manager.rest.service.filing.domain.exception.PatentCreationException;

import java.util.List;

public interface PatentRepository {

    Patent findByIpfs(String ipfs);

    Patent findByUniqueIdentifier(String uniqueIdentifier);

    Patent save(Patent patent);

    List<Patent> findAll();

}
