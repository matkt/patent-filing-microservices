package com.patent.manager.rest.service.filing.api.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(description = "Response DTO which contains IPFS id of the patent created ")
public class AddPatentResponse {
    @NotNull
    @NotEmpty
    @ApiModelProperty(notes = "IPFS id of the patent")
    String ipfs;
}
