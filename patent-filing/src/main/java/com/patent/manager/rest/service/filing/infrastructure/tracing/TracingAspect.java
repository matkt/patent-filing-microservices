package com.patent.manager.rest.service.filing.infrastructure.tracing;

import com.google.gson.Gson;
import io.opentracing.Span;
import io.opentracing.Tracer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.UUID;

@Aspect
@Component
public class TracingAspect {

    private static final String TRACE_ID = "traceId";
    private static final String PARAMS = "params";
    private static final String RESULT = "result";

    @Autowired
    Tracer tracer;

    @Autowired
    Gson gson;

    @Around("@annotation(Trace)")
    public Object traceMethod(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        Trace trace = method.getAnnotation(Trace.class);

        Span span = tracer
                .buildSpan(trace.value())
                .withTag(PARAMS, gson.toJson(joinPoint.getArgs()))
                .start();

        long start = System.currentTimeMillis();
        try {

            Object proceed = joinPoint.proceed();

            long executionTime = System.currentTimeMillis() - start;
            span.setTag(RESULT, gson.toJson(proceed));
            span.finish(executionTime);

            return proceed;
        } catch (Exception e) {

            long executionTime = System.currentTimeMillis() - start;
            span.setTag(RESULT, gson.toJson(e.getMessage()));
            span.finish(executionTime);

            throw e;
        }

    }
}
