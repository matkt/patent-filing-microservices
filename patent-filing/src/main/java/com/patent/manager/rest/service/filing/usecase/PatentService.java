package com.patent.manager.rest.service.filing.usecase;

import brave.Span;
import com.patent.manager.rest.service.filing.domain.*;
import com.patent.manager.rest.service.filing.domain.exception.PatentCreationException;
import com.patent.manager.rest.service.filing.domain.exception.PatentNotFoundException;
import com.patent.manager.rest.service.filing.domain.repository.FileRepository;
import com.patent.manager.rest.service.filing.domain.repository.PatentRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static org.bouncycastle.util.encoders.Hex.decode;
import static org.web3j.crypto.Hash.sha3String;

@Getter
@AllArgsConstructor
@Service
public class PatentService {

    @Autowired
    PatentRepository patentRepository;

    @Autowired
    FileRepository fileRepository;

    /**
     * Allows to retrieve patent stored in the database based on IPFS
     *
     * @param ipfs
     * @return patent found
     */
    public Patent getPatent(String ipfs) {
        Patent foundPatent = patentRepository.findByIpfs(ipfs);
        if (foundPatent != null) {
            return foundPatent;
        } else {
            throw new PatentNotFoundException(ipfs);
        }
    }


    /**
     * Allows to retrieve all patents stored in the database
     *
     * @return patents found
     */
    public List<Patent> getAllPatents() {
        List<Patent> foundPatents = patentRepository.findAll();
        if (foundPatents == null) {
            foundPatents = emptyList();
        }
        return foundPatents;
    }

    /**
     * Allows to add patent in the database
     *
     * @param name
     * @param content
     * @param description
     * @throws PatentCreationException
     */
    public String addPatent(String name, String content, String description) {
        String ipfs = fileRepository.sendFile(name, decode(content));
        if(patentRepository.findByIpfs(ipfs)==null) {
            patentRepository.save(
                    new Patent(sha3String(ipfs), ipfs, description)
            );
            return ipfs;
        }else{
            throw new PatentCreationException("already exist");
        }
    }

    /**
     * Bound patent stored in the database to the blockchain
     *
     * @param id                      sha3 of the ipfs
     * @param recordedTransactionHash
     * @throws PatentNotFoundException
     */
    public void linkPatentToBlockchain(String id, String recordedTransactionHash)
            throws PatentNotFoundException {
        Patent foundPatent = patentRepository.findByUniqueIdentifier(id);
        if (foundPatent != null) {
            foundPatent.linkToBlockchain(recordedTransactionHash);
            patentRepository.save(foundPatent);
        } else {
            throw new PatentNotFoundException(id);
        }
    }
}
