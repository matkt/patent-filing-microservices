package com.patent.manager.rest.service.filing.infrastructure.broker;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@ToString
/**
 * Message received via KAFKA
 */
public class Message<T> {

    /**
     * Type of the broker received (PATENT_RECORDED or PATENT_APPROVED)
     */
    private String messageType;
    /**
     * Id of the broker
     */
    private String id;
    /**
     * Sender of the broker
     */
    private String sender;
    /**
     * The date the broker was sent
     */
    private Date timestamp;
    /**
     * The transaction hash bound to this broker
     */
    private String transactionHash;
    /**
     * Additional paylod
     */
    private T payload;

}
