package com.patent.manager.rest.service.filing.infrastructure.repository;

import com.patent.manager.rest.service.filing.domain.repository.FileRepository;
import com.patent.manager.rest.service.filing.domain.exception.PatentCreationException;
import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

import static java.lang.String.format;

@Service
@Slf4j
public class IpfsFileRepository  implements FileRepository {

    @Value("${ipfs.hostname}")
    String hostname;

    @Value("${ipfs.port}")
    int port;

    private IPFS ipfs;

    @PostConstruct
    public void init()  {
        try {
            ipfs = new IPFS(hostname, port);
            ipfs.refs.local();
        } catch (Exception e){
            log.error(e.getMessage());
        }
    }

    /**
     * Allows to save a send file
     * @param fileName
     * @param data
     * @return
     * @throws PatentCreationException
     */
    @Override
    public String sendFile(String fileName, byte[] data) throws PatentCreationException {
        NamedStreamable.ByteArrayWrapper file
                = new NamedStreamable.ByteArrayWrapper(fileName, data);
        try {
            MerkleNode addResult = ipfs.add(file).get(0);
            log.debug("ipfs result " + addResult.hash.toString());
            return addResult.hash.toString();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new PatentCreationException(fileName);
        }
    }

}
