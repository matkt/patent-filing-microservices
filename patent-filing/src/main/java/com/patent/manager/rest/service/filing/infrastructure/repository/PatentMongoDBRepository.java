package com.patent.manager.rest.service.filing.infrastructure.repository;

import com.patent.manager.rest.service.filing.domain.Patent;
import com.patent.manager.rest.service.filing.domain.repository.PatentRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PatentMongoDBRepository extends PatentRepository, MongoRepository<Patent,String> {

}
