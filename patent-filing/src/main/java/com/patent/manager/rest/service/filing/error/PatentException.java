package com.patent.manager.rest.service.filing.error;

public abstract class PatentException extends RuntimeException {

    protected PatentException(String message) {
        super(message);
    }

    public PatentApiError apiError() {
        return new PatentApiError(this.getClass().getSimpleName(), this.getMessage());
    }

}
