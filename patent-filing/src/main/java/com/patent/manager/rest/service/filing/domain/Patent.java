package com.patent.manager.rest.service.filing.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(collection = "patents")
@ApiModel(description = "Entity representing a patent tracked by the application")
public class Patent {

    @Id
    @NotBlank
    @ApiModelProperty(notes = "Unique identifier of the patent in the blockchain. No two patent can have the same id.")
    String uniqueIdentifier;
    @UniqueElements
    @NotBlank
    @ApiModelProperty(notes = "InterPlanetary File System identifier")
    String ipfs;
    @NotBlank
    @Size(min = 46)
    @ApiModelProperty(notes = "description of the patent")
    String patentDescription;
    @ApiModelProperty(notes = "patent recorded transaction hash in the blockchain")
    String recordedTransactionHash;
    @ApiModelProperty(notes = "patent approved transaction hash in the blockchain")
    String approvedTransactionHash;

    /**
     * Constructor of Patent
     *
     * @param uniqueIdentifier
     * @param ipfs
     * @param patentDescription
     */
    public Patent(@NotBlank String uniqueIdentifier,
                  @UniqueElements @NotBlank String ipfs,
                  @NotBlank @Size(min = 46) String patentDescription) {
        this.uniqueIdentifier = uniqueIdentifier;
        this.ipfs = ipfs;
        this.patentDescription = patentDescription;
    }

    /**
     * Bound patent stored in the database to the blockchain
     * @param recordedTransactionHash
     */
    public void linkToBlockchain(String recordedTransactionHash) {
        this.recordedTransactionHash = recordedTransactionHash;
    }

    /**
     * Approve patent via the blockchain
     * @param approvedTransactionHash
     */
    public void approve(String approvedTransactionHash) {
        this.approvedTransactionHash = approvedTransactionHash;
    }
}
