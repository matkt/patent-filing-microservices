package com.patent.manager.rest.service.filing.domain.exception;

import com.patent.manager.rest.service.filing.error.PatentException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PatentNotFoundException extends PatentException {

    private static final String MESSAGE_TEMPLATE = "patent %s was not found";

    public PatentNotFoundException(String id) {
        super(format(MESSAGE_TEMPLATE, id));
    }
}
