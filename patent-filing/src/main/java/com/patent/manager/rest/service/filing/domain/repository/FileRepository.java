package com.patent.manager.rest.service.filing.domain.repository;

import com.patent.manager.rest.service.filing.domain.exception.PatentCreationException;

public interface FileRepository {

    String sendFile(String fileName, byte[] data) throws PatentCreationException;

}
