package com.patent.manager.rest.service.filing.error;

public class PatentApiError {
    private String type;
    private String message;

    public PatentApiError(String type, String message) {
        this.type = type;
        this.message = message;
    }

}
