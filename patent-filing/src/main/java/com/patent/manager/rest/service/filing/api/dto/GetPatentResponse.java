package com.patent.manager.rest.service.filing.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(description = "Response DTO which contains all the information of the stored patent")
public class GetPatentResponse {

    @NotBlank
    @ApiModelProperty(notes = "Unique identifier of the patent in the blockchain. No two patent can have the same id.")
    String uniqueIdentifier;
    @UniqueElements
    @NotBlank
    @ApiModelProperty(notes = "InterPlanetary File System identifier")
    String ipfs;
    @NotBlank
    @Size(min = 46)
    @ApiModelProperty(notes = "description of the patent")
    String patentDescription;
    @ApiModelProperty(notes = "patent recorded transaction hash in the blockchain")
    String recordedTransactionHash;
    @ApiModelProperty(notes = "patent approved transaction hash in the blockchain")
    String approvedTransactionHash;

}
