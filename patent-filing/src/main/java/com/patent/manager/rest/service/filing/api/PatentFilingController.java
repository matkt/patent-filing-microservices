package com.patent.manager.rest.service.filing.api;

import com.patent.manager.rest.service.filing.api.dto.AddPatentRequest;
import com.patent.manager.rest.service.filing.api.dto.AddPatentResponse;
import com.patent.manager.rest.service.filing.api.dto.GetPatentResponse;
import com.patent.manager.rest.service.filing.domain.Patent;
import com.patent.manager.rest.service.filing.infrastructure.tracing.Trace;
import com.patent.manager.rest.service.filing.usecase.PatentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.lang.String.format;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
@Api(description = "Set of endpoints for Creating and Retrieving Patents.")
public class PatentFilingController {

    @Autowired
    PatentService patentServiceManager;

    @Autowired
    DozerBeanMapper dozerBeanMapper;

    @RequestMapping(value = "/getPatent", method = GET)
    @ApiOperation("Allow to retrieve a patent with a given ipfs")
    @Trace("Get Patent")
    public GetPatentResponse getPatent(@RequestParam(value = "ipfs") String ipfs) {
        log.debug(format("getPatent for ipfs %s", ipfs));
        Patent patent = patentServiceManager.getPatent(ipfs);
        log.debug(format("patent found %s", patent));
        return dozerBeanMapper.map(patent, GetPatentResponse.class);
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllPatents", method = GET)
    @ApiOperation("Allow to retrieve all patents")
    @Trace("Get All Patent")
    public List<Patent> getAllPatents() {
        log.debug(format("getAllPatents"));
        List<Patent> patents = patentServiceManager.getAllPatents();
        log.debug(format("%s patents found", patents.size()));
        return patents;
    }

    @CrossOrigin
    @RequestMapping(value = "/addPatent", method = POST)
    @ApiOperation("Allow to add a new patent.")
    @Trace("Add Patent")
    public AddPatentResponse addPatent(@RequestBody @Valid AddPatentRequest addPatentRequest) {
        log.debug(format("add Patent for name %s", addPatentRequest.getName()));
        String ipfs = patentServiceManager.addPatent(
                addPatentRequest.getName(),
                addPatentRequest.getContent(),
                addPatentRequest.getDescription()
        );
        log.debug(format("patent added for name %s", addPatentRequest.getName()));
        return new AddPatentResponse(ipfs);
    }

}
