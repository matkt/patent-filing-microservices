package com.dejamobile.manager.event.watcher;

import com.dejamobile.manager.event.watcher.domain.EventService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EventWatcherManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventWatcherManagementApplication.class, args);
    }


    @Bean
    public EventService eventService(){
        return new EventService();
    }

    @Bean
    public Gson gson(){
        return new GsonBuilder().create();
    }
}
