package com.dejamobile.manager.event.watcher.domain;

public interface EventWatcher {

    void startListeningBlockchainEvent();

}
