package com.dejamobile.manager.event.watcher.domain;



import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Slf4j
public class EventService {

    @Autowired
    EventWatcher eventWatcher;

    @PostConstruct
    void startListeningBlockchainEvent(){
        eventWatcher.startListeningBlockchainEvent();
    }

}
