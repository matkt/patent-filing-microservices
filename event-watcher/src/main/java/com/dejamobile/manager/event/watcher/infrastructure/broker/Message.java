package com.dejamobile.manager.event.watcher.infrastructure.broker;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Message<T> {

    private String messageType;
    private String id = UUID.randomUUID().toString();
    private String sender = "EventWatcher";
    private Date timestamp = new Date();
    private String transactionHash;

    private T payload;

    public Message(MessageType type, String transactionHash, T payload) {
        this.messageType = type.name();
        this.transactionHash = transactionHash;
        this.payload = payload;
    }

}
