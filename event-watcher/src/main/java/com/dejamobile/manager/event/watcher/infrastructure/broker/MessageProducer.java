package com.dejamobile.manager.event.watcher.infrastructure.broker;


import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
@Slf4j
public class MessageProducer {

    private final KafkaTemplate<Object, Message> kafkaTemplate;

    MessageProducer(KafkaTemplate<Object, Message> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(Message message) {
        this.kafkaTemplate.send("patentTopic", message);
        log.debug(format("Sent broker [ %s ]", message));
    }

}
