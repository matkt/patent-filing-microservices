package com.dejamobile.manager.event.watcher.infrastructure.blockchain;


import com.dejamobile.manager.event.watcher.domain.EventWatcher;
import com.dejamobile.manager.event.watcher.infrastructure.broker.Message;
import com.dejamobile.manager.event.watcher.infrastructure.broker.MessageProducer;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Bytes32;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.http.HttpService;

import java.util.Arrays;
import java.util.List;

import static com.dejamobile.manager.event.watcher.infrastructure.broker.MessageType.PATENT_APPROVED;
import static com.dejamobile.manager.event.watcher.infrastructure.broker.MessageType.PATENT_RECORDED;
import static java.lang.String.format;

@Service
@Slf4j
public class Web3JEventWatcher implements EventWatcher {

    @Autowired
    MessageProducer messageSender;

    @Autowired
    Gson gson;

    @Value("${rpc.hostname}")
    String hostname;

    @Value("${rpc.port}")
    String port;

    private Web3j web3j;

    @Override
    public void startListeningBlockchainEvent() {
        log.debug(format("start listening event %s %s", hostname, port));

        web3j = Web3j.build(new HttpService(format("http://%s:%s", hostname, port)));

        EthFilter filterPatentRecorded = new EthFilter();
        Event eventPatentRecorded = new Event("NewPatentRecorded",
                Arrays.asList(new TypeReference<Address>(true) {
                }, new TypeReference<Bytes32>() {
                }));
        String topicEventPatentRecorded = EventEncoder.encode(eventPatentRecorded);
        filterPatentRecorded.addSingleTopic(topicEventPatentRecorded);

        EthFilter filterPatentApproved = new EthFilter();
        Event eventPatentApproved = new Event("NewPatentApproved",
                Arrays.asList(new TypeReference<Address>(true) {
                }, new TypeReference<Bytes32>() {
                }));
        String topicEventPatentApproved = EventEncoder.encode(eventPatentApproved);
        filterPatentApproved.addSingleTopic(topicEventPatentApproved);

        web3j.ethLogObservable(filterPatentApproved).subscribe(logEvent -> {
            log.debug("event patent approved");
            String idPatent = Hex.encodeHexString(((Bytes32) extractEventParameters(eventPatentApproved, logEvent).get(0)).getValue());
            log.debug(String.format("Id Patent %s", idPatent));
            messageSender.send(new Message<>(PATENT_APPROVED, logEvent.getTransactionHash(), idPatent));
        });

        web3j.ethLogObservable(filterPatentRecorded).subscribe(logEvent -> {
            log.debug("event patent recorded");
            String idPatent = Hex.encodeHexString(((Bytes32) extractEventParameters(eventPatentRecorded, logEvent).get(0)).getValue());
            log.debug(String.format("Id Patent %s", idPatent));
            messageSender.send(new Message<>(PATENT_RECORDED, logEvent.getTransactionHash(), idPatent));
        });
    }

    private static List<Type> extractEventParameters(Event event, Log log) {

        List<Type> values = FunctionReturnDecoder.decode(log.getData(), event.getNonIndexedParameters());

        List<TypeReference<Type>> indexedParameters = event.getIndexedParameters();
        for (int i = 0; i < indexedParameters.size(); i++) {
            Type value = FunctionReturnDecoder.
                    decodeIndexedValue(
                            Hex.encodeHexString(log.getTopics().get(i + 1).getBytes()),
                            indexedParameters.get(i)
                    );
            values.add(value);
        }
        return values;
    }

}
