package com.dejamobile.manager.event.watcher.infrastructure.broker;

public enum  MessageType {

    PATENT_APPROVED,
    PATENT_RECORDED;

}
